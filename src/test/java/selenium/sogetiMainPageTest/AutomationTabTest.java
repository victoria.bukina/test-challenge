package selenium.sogetiMainPageTest;

import selenium.pages.BasePage;
import selenium.pages.MainPage;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AutomationTabTest
{

    WebDriver driver;

    @BeforeMethod
    public void openSogetiSite()
    {
        System.setProperty("webdriver.chrome.driver", "\\IDEAProjects\\test-challenge\\Driver\\Chrome\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("https://www.sogeti.com/");
        BasePage.setDriver(driver);
    }

    @AfterMethod
    public void closeBrowser()
    {
        if (driver != null)
        {
            driver.quit();
        }
    }

    // There are only 2 first ui tests. The third one is OneClassSeleniumTests class.

    @Test
    public void testCase1() throws InterruptedException
    {
        TimeUnit.SECONDS.sleep(2);
        MainPage mainPage = new MainPage();
        mainPage.clickAcceptAllButton();
        mainPage.areBaseElementsPresent();
        mainPage.hoverElementServicesAndCheckInitialColor();
        mainPage.clickOnHoverLinkAndCheckColor();
    }

    @Test
    public void testCase2() throws InterruptedException
    {
        TimeUnit.SECONDS.sleep(2);
        MainPage mainPage = new MainPage();
        mainPage.clickAcceptAllButton();
        mainPage.areBaseElementsPresent();
        mainPage.clickOnHoverLinkAndCheckColor();
        mainPage.scrollToContactUs();
        mainPage.fillFormFields();
        /**Unable to check sending form without check checkbox Captcha
         *Fields are filled in with random data
         * Here I have negative selenium.test, I check that I can't send message without Captcha
         * Selenium is Robot, and captcha is checking if you are robot or not
         */

    }

}