package selenium.allInOneClass;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.github.javafaker.Faker;

import static org.openqa.selenium.support.ui.ExpectedConditions.numberOfWindowsToBe;
import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;

@Slf4j
@Test
public class OneClassSeleniumTests
{

    WebDriver driver;
    private static final By acceptAllButton    = By.xpath(
            "//div[@class='consentcontent']//div[@class='buttons']//button[@class='acceptCookie']");
    private static final By headerH1           = By.xpath("//div[@class='page-heading']//span[contains(text(), 'Automation')]");
    private static final By header             = By.xpath("//div[@class='case-box-wrapper']//div[contains(text(), 'AUTOMATION')]");
    private static final By isSelectedMenuItem = By.xpath(
            "//li[@class='selected  current expanded']//a[@class='subMenuLink' and contains(text(), 'Automation')]");

    private static final By contactUsElement = By.xpath("//h2[@class='Form__Title' and contains(text(), 'Contact us:') ]");
    private static final By firstName        = By.xpath("//input[@name='__field_123927']");
    private static final By lastName         = By.xpath("//input[@name='__field_123938']");
    private static final By email            = By.xpath("//input[@name='__field_123928']");
    private static final By phone            = By.xpath("//input[@name='__field_123929']");
    private static final By company          = By.xpath("//input[@name='__field_132738']");
    private static final By message          = By.xpath("//textarea[@name='__field_123931']");
    private static final By country          = By.xpath("//select[@name='__field_132596']");
    //    private static final By germany          = By.xpath("//option[@value='Germany']");
    private static final By iAgreeCheckbox   = By.xpath("//label[@for='__field_1239350']");
    private static final By buttonSubmit     = By.xpath("//button[@name='submit']");
    private static final By errormessage     = By.xpath(("//span[@role='alert' and contains(text(), 'Invalid captcha value.')]"));

    private static final By worlWideListArrow = By.xpath("//div[@class='sprite-header sprite-global-arrowdown']");
    private static final By worldWideList     = By.xpath("//div[@id='country-list-id']");
    private static final By france            = By.xpath(
            "//div[@id='country-list-id']//a[@href='https://www.fr.sogeti.com/' and contains(text(), 'France')]");
    private static final By belgium           = By.xpath(
            "//div[@id='country-list-id']//a[@href='https://www.sogeti.be/' and contains(text(), 'Belgium')]");
    private static final By finland           = By.xpath(
            "//div[@id='country-list-id']//a[@href='https://www.sogeti.fi/' and contains(text(), 'Finland')]");
    private static final By germany           = By.xpath(
            "//div[@id='country-list-id']//a[@href='https://www.sogeti.de/' and contains(text(), 'Germany')]");
    private static final By ireland           = By.xpath(
            "//div[@id='country-list-id']//a[@href='https://www.sogeti.ie/' and contains(text(), 'Ireland')]");
    private static final By luxemburg         = By.xpath(
            "//div[@id='country-list-id']//a[@href='https://www.sogeti.lu/' and contains(text(), 'Luxembourg')]");
    private static final By netherlands       = By.xpath(
            "//div[@id='country-list-id']//a[@href='https://www.sogeti.nl/' and contains(text(), 'Netherlands')]");
    private static final By norway            = By.xpath(
            "//div[@id='country-list-id']//a[@href='https://www.sogeti.no/' and contains(text(), 'Norway')]");
    private static final By spain             = By.xpath(
            "//div[@id='country-list-id']//a[@href='https://www.sogeti.es/' and contains(text(), 'Spain')]");
    private static final By sweden            = By.xpath(
            "//div[@id='country-list-id']//a[@href='https://www.sogeti.se/' and contains(text(), 'Sweden')]");
    private static final By uk                = By.xpath(
            "//div[@id='country-list-id']//a[@href='https://www.uk.sogeti.com/' and contains(text(), 'UK')]");
    private static final By us                = By.xpath(
            "//div[@id='country-list-id']//a[@href='https://www.us.sogeti.com/' and contains(text(), 'USA')]");

    //    private static final By logoImage       = By.xpath("//div[@class='logo1']");
    //    private static final By mainMenu        = By.xpath("//div[@class='desktop_wrapper']//ul[@class='level0 clearfix']");

    @BeforeMethod
    public void openSogetiSite(ITestContext iTestContext)
    {
        System.setProperty("webdriver.chrome.driver", "\\IDEAProjects\\test-challenge\\Driver\\Chrome\\chromedriver.exe");
        driver = new ChromeDriver();
        iTestContext.setAttribute("driver", driver);
        //        chromeWebDriver.manage().window().setSize(new Dimension(1920, 1080));
        driver.manage().window().maximize();
        driver.navigate().to("https://www.sogeti.com/");
    }

    @AfterMethod
    public void closeBrowser()
    {
        driver.quit();
    }

    @Test
    @Description("Open main page, choose Automation Tab and check that it is opened")
    public void testCase1()
    {
        //        Screenshots.takeScreenshot(chromeWebDriver, "./target/screenshots/");
        clickAcceptAllButton();
        areBaseElementsPresent();
        hoverElementServicesAndCheckInitialColor();
        clickOnHoverLinkAndCheckColor();

    }

    @Test
    @Description("Open main page, choose Automation Tab and fill form Contact Us")
    public void testCase2()
    {
        clickAcceptAllButton();
        areBaseElementsPresent();
        clickOnHoverLinkAndCheckColor();
        scrollToContactUs();
        fillFormFields();
        /**Unable to check sending form without check checkbox Captcha
         *Fields are filled in with random data
         * Here I have negative selenium.test, I check that I can't send message without Captcha
         * Selenium is Robot, and captcha is checking if you are robot or not
         */
    }

    @Test
    @Description("Check that all the Country specific Sogeti links are working")
    public void testCase3()
    {

        clickAcceptAllButton();
        areBaseElementsPresent();
        switchToNewTabAndCheckParameters("Sogeti España", spain);
        switchToNewTabAndCheckParameters("Sogeti Belgium", belgium);
        switchToNewTabAndCheckParameters("Sogeti Finland", finland);
        switchToNewTabAndCheckParameters("Sogeti Deutschland GmbH – Beratungsdienstleistungen für Softwaretest und Qualitätssicherung",
                germany);
        switchToNewTabAndCheckParameters("Sogeti Ireland", ireland);
        switchToNewTabAndCheckParameters("Sogeti Luxembourg", luxemburg);
        switchToNewTabAndCheckParameters("Sogeti Norge", norway);
        switchToNewTabAndCheckParameters("Sogeti Sverige", sweden);
        switchToNewTabAndCheckParameters("Sogeti USA", us);
        switchToNewTabAndCheckParameters(
                "Sogeti UK | Software Testing Services, Digital Services, DevOps Services, DevOps Consultancy, Testing Consultancy",
                uk);
        switchToNewTabAndCheckParameters("We Make Technology Work | Sogeti", netherlands);
        switchToNewTabAndCheckParameters("Sogeti France | Gérez la transformation numérique de votre entreprise avec Sogeti", france);

    }

    @Step("Accept all cookies")
    public void clickAcceptAllButton()
    {
        driver
                .findElement(acceptAllButton)
                .click();
        log.info("All cookies were accepted");
    }

    @Step("Check basic elements are displayed")
    public void areBaseElementsPresent()
    {

        //        Assert.assertTrue(chromeWebDriver.findElement(logoImage).isDisplayed(), "Logo image isn't displayed");
        //        Assert.assertTrue(chromeWebDriver.findElement(mainMenu).isDisplayed(), "Main menu isn't displayed");

        WebElement logoImage = driver
                .findElement(By.xpath("//div[@class='logo1']"));
        Assert.assertTrue(logoImage.isDisplayed(), "Logo image isn't displayed");

        WebElement mainMenu = driver
                .findElement(By.xpath("//div[@class='desktop_wrapper']//ul[@class='level0 clearfix']"));
        Assert.assertTrue(mainMenu.isDisplayed(), "Main menu isn't displayed");

    }

    @Step("Hover element and check color of link")
    private void hoverElementServicesAndCheckInitialColor()
    {
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(
                By.xpath("//div[@class='desktop_wrapper']//div[@class='wrapper']//span[contains(text(), 'Services')]"))).perform();
        action.moveToElement(driver.findElement(By.xpath("//a[@class='subMenuLink' and contains(text(), 'Automation')]")))
                .build().perform();
        WebElement initialLink = driver
                .findElement(By.xpath("//a[@class='subMenuLink' and contains(text(), 'Automation')]"));

        String expectedColorBeforeClick = "#0070ad";
        String s1 = initialLink.getCssValue("color");
        String actualColorBeforeClick = Color.fromString(s1).asHex();
        Assert.assertEquals(actualColorBeforeClick, expectedColorBeforeClick);
    }

    @Step("Hover link Automation, click on it, hover again and chek color of link and Automation Screen is displayed")
    public void clickOnHoverLinkAndCheckColor()
    {
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(
                By.xpath("//div[@class='desktop_wrapper']//div[@class='wrapper']//span[contains(text(), 'Services')]"))).perform();
        action.moveToElement(driver.findElement(By.xpath("//a[@class='subMenuLink' and contains(text(), 'Automation')]"))).click()
                .build().perform();
        WebElement initialLink = driver
                .findElement(By.xpath("//a[@class='subMenuLink' and contains(text(), 'Automation')]"));
        //        Assert.assertTrue(chromeWebDriver.findElement(isSelectedMenuItem).isDisplayed(), "Service and Automation link are not selected");
        String s2 = initialLink.getCssValue("color");
        String actualColorAfterClick = Color.fromString(s2).asHex();
        String expectedColorAfterClick = "#ff304c";
        Assert.assertEquals(actualColorAfterClick, expectedColorAfterClick);
        Assert.assertTrue(driver.findElement(headerH1).isDisplayed(), "Automation article header is not displayed");
        Assert.assertTrue(driver.findElement(header).isDisplayed(), "Automation header is not displayed");
        String actualUrl = driver.getCurrentUrl();
        String expectedUrl = "https://www.sogeti.com/services/automation/";
        Assert.assertEquals(actualUrl, expectedUrl);

    }

    @Step("Scroll to Form contact Us")
    public void scrollToContactUs()
    {
        WebElement element = driver
                .findElement(contactUsElement);
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
    }

    @Step("Fill Form fields and send message")
    public void fillFormFields()
    {
        Faker faker = new Faker();
        driver.findElement(firstName).sendKeys(faker.name().firstName());
        driver.findElement(lastName).sendKeys(faker.name().lastName());
        driver.findElement(email).sendKeys(faker.name().firstName() + "." + faker.name().lastName() + "@gmail.com");
        driver.findElement(phone).sendKeys(faker.phoneNumber().toString());
        driver.findElement(company).sendKeys(faker.company().toString());
        driver.findElement(message).sendKeys(faker.shakespeare().kingRichardIIIQuote());
        Select countries = new Select(driver.findElement(By.xpath("//select[@name='__field_132596']")));
        countries.selectByValue("Germany");
        driver.findElement(iAgreeCheckbox).click();
        //        chromeWebDriver.findElement(By.xpath("//div[@class='recaptcha-checkbox-border']")).click();
        driver.findElement(buttonSubmit).click();
        Assert.assertTrue(driver.findElement(errormessage).isDisplayed(), "Error message didn't appear");
    }

    @Step("Switch to new Tab")
    public void switchToNewTabAndCheckParameters(String title, By element)
    {
        String originalWindow = driver.getWindowHandle();
        assert driver.getWindowHandles().size() == 1;
        driver.findElement(worlWideListArrow).click();
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(element)).click().build().perform();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        //        chromeWebDriver.findElement(By.linkText("new tab")).click();
        wait.until(numberOfWindowsToBe(2));
        for (String windowHandle : driver.getWindowHandles())
        {
            if (!originalWindow.contentEquals(windowHandle))
            {
                driver.switchTo().window(windowHandle);
                break;
            }

        }
        wait.until(titleIs(title));

        driver.close();
        wait.until(numberOfWindowsToBe(1));
        driver.switchTo().window(originalWindow);
        String actualStatus = driver.findElement(worldWideList).getAttribute("style");
        //        if (actualStatus.equals("display: none;"))
        //        {
        driver.findElement(worlWideListArrow).click();
        //        }
    }
}
