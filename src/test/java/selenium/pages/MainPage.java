package selenium.pages;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.github.javafaker.Faker;

@Slf4j
public class MainPage extends BasePage
{
    private static final By logoImage       = By.xpath("//div[@class='logo1']");
    private static final By acceptAllButton = By.xpath(
            "//div[@class='consentcontent']//div[@class='buttons']//button[@class='acceptCookie']");
    private static final By mainMenu        = By.xpath("//div[@class='desktop_wrapper']//ul[@class='level0 clearfix']");
    //    private static final By services      = By.xpath(
    //            "//div[@class='desktop_wrapper']//div[@class='wrapper']//span[contains(text(), 'Services')]");\
    private static final By services        = By.xpath("//li[@class='has-children   level2 focus-style']");
    private static final By automationTab   = By.xpath(
            "//div[@class='mega-navbar refreshed level2']//span[@class='heading']//ul[@class='level1']//a[contains(text(), 'Automation')]");

    //    List<WebElement> elements = driver.findElements(By.xpath("//div[@class='level0 clearfix']"));
    private static final By headerH1 = By.xpath("//div[@class='page-heading']//span[contains(text(), 'Automation')]");
    private static final By header   = By.xpath("//div[@class='case-box-wrapper']//div[contains(text(), 'AUTOMATION')]");

    private static final By contactUsElement = By.xpath("//h2[@class='Form__Title' and contains(text(), 'Contact us:') ]");
    private static final By firstName        = By.xpath("//input[@name='__field_123927']");
    private static final By lastName         = By.xpath("//input[@name='__field_123938']");
    private static final By email            = By.xpath("//input[@name='__field_123928']");
    private static final By phone            = By.xpath("//input[@name='__field_123929']");
    private static final By company          = By.xpath("//input[@name='__field_132738']");
    private static final By message          = By.xpath("//textarea[@name='__field_123931']");
    private static final By country          = By.xpath("//select[@name='__field_132596']");
    //    private static final By germany          = By.xpath("//option[@value='Germany']");
    private static final By iAgreeCheckbox   = By.xpath("//label[@for='__field_1239350']");
    private static final By buttonSubmit     = By.xpath("//button[@name='submit']");
    private static final By errormessage     = By.xpath(("//span[@role='alert' and contains(text(), 'Invalid captcha value.')]"));

    public void clickAcceptAllButton()
    {
        driver.findElement(acceptAllButton).click();
    }

    public void areBaseElementsPresent()
    {
        driver.findElement(logoImage).isDisplayed();
        driver.findElement(mainMenu).isDisplayed();
        log.info("Basic elements are displayed");
    }

    public void hoverElementServicesAndCheckInitialColor()
    {
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(
                By.xpath("//div[@class='desktop_wrapper']//div[@class='wrapper']//span[contains(text(), 'Services')]"))).perform();
        action.moveToElement(driver.findElement(By.xpath("//a[@class='subMenuLink' and contains(text(), 'Automation')]")))
                .build().perform();
        WebElement initialLink = driver
                .findElement(By.xpath("//a[@class='subMenuLink' and contains(text(), 'Automation')]"));

        String expectedColorBeforeClick = "#0070ad";
        String s1 = initialLink.getCssValue("color");
        String actualColorBeforeClick = Color.fromString(s1).asHex();
        Assert.assertEquals(actualColorBeforeClick, expectedColorBeforeClick);
    }

    //    ("Hover link Automation, click on it, hover again and chek color of link and Automation Screen is displayed")
    public void clickOnHoverLinkAndCheckColor()
    {
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(
                By.xpath("//div[@class='desktop_wrapper']//div[@class='wrapper']//span[contains(text(), 'Services')]"))).perform();
        action.moveToElement(driver.findElement(By.xpath("//a[@class='subMenuLink' and contains(text(), 'Automation')]"))).click()
                .build().perform();
        WebElement initialLink = driver
                .findElement(By.xpath("//a[@class='subMenuLink' and contains(text(), 'Automation')]"));
        //        Assert.assertTrue(chromeWebDriver.findElement(isSelectedMenuItem).isDisplayed(), "Service and Automation link are not selected");
        String s2 = initialLink.getCssValue("color");
        String actualColorAfterClick = Color.fromString(s2).asHex();
        String expectedColorAfterClick = "#ff304c";
        Assert.assertEquals(actualColorAfterClick, expectedColorAfterClick);
        Assert.assertTrue(driver.findElement(headerH1).isDisplayed(), "Automation article header is not displayed");
        Assert.assertTrue(driver.findElement(header).isDisplayed(), "Automation header is not displayed");
        String actualUrl = driver.getCurrentUrl();
        String expectedUrl = "https://www.sogeti.com/services/automation/";
        Assert.assertEquals(actualUrl, expectedUrl);

    }

    public void scrollToContactUs()
    {
        WebElement element = driver
                .findElement(contactUsElement);
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
    }

    public void fillFormFields() throws InterruptedException
    {
        Faker faker = new Faker();
        driver.findElement(firstName).sendKeys(faker.name().firstName());
        driver.findElement(lastName).sendKeys(faker.name().lastName());
        driver.findElement(email).sendKeys(faker.name().firstName() + "." + faker.name().lastName() + "@gmail.com");
        driver.findElement(phone).sendKeys(faker.phoneNumber().toString());
        driver.findElement(company).sendKeys(faker.company().toString());
        driver.findElement(message).sendKeys(faker.shakespeare().kingRichardIIIQuote());
        Select countries = new Select(driver.findElement(By.xpath("//select[@name='__field_132596']")));
        countries.selectByValue("Germany");
        driver.findElement(iAgreeCheckbox).click();
        //        chromeWebDriver.findElement(By.xpath("//div[@class='recaptcha-checkbox-border']")).click();
        driver.findElement(buttonSubmit).click();
        TimeUnit.SECONDS.sleep(2);
        Assert.assertTrue(driver.findElement(errormessage).isDisplayed(), "Error message didn't appear");
        //The error message will work only few times. After that we will see confirmation images and I can't automate it.
    }

}
