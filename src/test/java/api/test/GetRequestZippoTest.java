package api.test;

import io.qameta.allure.Description;
import io.restassured.RestAssured;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import static api.utils.Specifications.requestSpecification;
import static api.utils.Specifications.responseSpecificationScOk;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItem;

public class GetRequestZippoTest
{
    public static final String baseHost = "http://api.zippopotam.us/de/bw/stuttgart";

    @Test
    @Description("Verify in Response - That country is Germany and state is Baden-Württemberg.")
    public void getResponseCheckJsonBodyParameters()
    {
        RestAssured.given()
                .spec(requestSpecification())
                .when()
                .get(baseHost)
                .then()
                .spec(responseSpecificationScOk())
                .assertThat()
                .body("country", Matchers.is("Germany"))
                .body("state", Matchers.is("Baden-Württemberg"));

    }

    @Test
    @Description("Verify in Response - For Post Code 70597 the place name has Stuttgart Degerloch.")
    public void getResponseJsonBody()
    {

        RestAssured.given()
                .spec(requestSpecification())
                .when().get(baseHost)
                .then().spec(responseSpecificationScOk())
                .body("places", hasItem(
                        allOf(
                                hasEntry("post code", "70597"), hasEntry("place name", "Stuttgart Degerloch"))));

    }

}



