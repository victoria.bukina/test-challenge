package api.test;

import io.qameta.allure.Description;
import io.restassured.RestAssured;

import org.testng.annotations.Test;

import static api.utils.Specifications.requestSpecification;
import static api.utils.Specifications.responseSpecificationScOk;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItem;

public class GetRequestDdtTest
{
    public static final String baseHost     = "http://api.zippopotam.us/";
    public static final String baverlyHills = "us/90210";
    public static final String schenectady  = "us/12345";
    public static final String waverley     = "ca/B2R";

    @Test
    @Description("Verify in Response - Place Name Beverly Hills is for input us and 90210.")
    public void getResponseBeverllyHills()
    {
        RestAssured.given()
                .spec(requestSpecification())
                .when()
                .baseUri(baseHost)
                .get(baverlyHills)
                .then()
                .spec(responseSpecificationScOk())
                .body("places", hasItem(
                        allOf(
                                hasEntry("place name", "Beverly Hills"))));
    }

    @Test
    @Description("Verify in Response - Place Name Schenectady is for input us and B2R.")
    public void getResponseSchenectady()
    {
        RestAssured.given()
                .spec(requestSpecification())
                .when()
                .baseUri(baseHost)
                .get(schenectady)
                .then()
                .spec(responseSpecificationScOk())
                .body("places", hasItem(
                        allOf(
                                hasEntry("place name", "Schenectady"))));
    }

    @Test
    @Description("Verify in Response - Place Name Waverley is for input ca and 12345.")
    public void getResponseWaverley()
    {
        RestAssured.given()
                .spec(requestSpecification())
                .when()
                .baseUri(baseHost)
                .get(waverley)
                .then()
                .spec(responseSpecificationScOk())
                .body("places", hasItem(
                        allOf(
                                hasEntry("place name", "Waverley"))));
    }
}
