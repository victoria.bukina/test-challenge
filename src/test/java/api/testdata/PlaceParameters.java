package api.testdata;

import io.qameta.allure.internal.shadowed.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlaceParameters
{
    public String placeName;
    public String postcode;

    public PlaceParameters(final String placeName, final String postcode)
    {
        this.placeName = placeName;
        this.postcode = postcode;
    }
}
