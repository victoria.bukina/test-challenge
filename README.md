# Test Challenge

## Getting started

You need to have last version of chromedriver <br><br>
\\test-challenge\Driver\Chrome\chromedriver.exe
<br><br>
There are two test classes for UI tests and two classes for API tetst<br><br>
>For UI:
OneClassSeleniumTests contains all 3 testcases
AutomationTabTest contains only 2 first testcases

>For API:
GetRequestZippoTest contains first API testcase
GetRequestDdtTest contains second API testcase


For UI tests I used Java 11+Selenium
For API tests I used RestAssured
```
cd existing_repo
git remote add origin https://gitlab.com/victoria.bukina/selenium.allInOneClass-challenge.git
git branch -M main
git push -uf origin main
```

Automation Task Guidelines
Please prepare the following use cases and upload them to a public repository (e.g. GitHub or
Bitbucket).
Make sure that test scripts are executable on a local machine. Add additional information in case any
software/framework is required to run those test cases (installation script is preferred).
Also, advice in form of a short README how to execute those test scripts.
###UI Tests
Using any test automation framework and any programming language (except play/record tools),
please do following:
####Test Case 1
1. Navigate to the URL https://www.sogeti.com/
2. Hover over Services Link (see Image below) and then Click Automation link.

3. Verify that Automation Screen is displayed, and “Automation” text is visible in Page.
4. Hover again over Services Link. Verify that the Services and Automation are selected (see
   Image below).

####Test Case 2
1. Navigate to the URL https://www.sogeti.com/
2. Hover over Services Link and then Click Automation link.
3. On Automation Page, scroll down to the Contact us Form.
4. Fill the First Name, Last Name, Email, Phone and Message fields with Random Generated
   Data.
5. Check the I agree checkbox.
6. Then Click SUBMIT button.
7. After clicking SUBMIT button the form is submitted and Thank you message is displayed.
   Assert the Thank you message.
### Note: While automating a Test Case, if you are encounter a test step which cannot be Automated.
Automate the Test till that Step and please explain in comments in why it is not possible to automate
the Test Step? What are the possible workarounds to Test such Test Case?

####Test Case 3
1. Navigate to the URL https://www.sogeti.com/
2. Click the Worldwide Dropdown link in Page Header.

3. A Country dropdown list is displayed. These are the Country specific Sogeti links.
4. Assert that all the Country specific Sogeti links are working.

###API Tests
1. Write API Test for http://api.zippopotam.us/de/bw/stuttgart
   • Verify that the response status code is 200 and content type is JSON.
   • Verify that the response time is below 1s.
   • Verify in Response - That "country" is "Germany" and "state" is "Baden-Württemberg".
   • Verify in Response - For Post Code "70597" the place name has "Stuttgart Degerloch".

2. Write API Data Driven Test for http://api.zippopotam.us/{country}/{postal-code}
   In Data Driven Test - replace "Country" and "Postal Code" in above Test URL from Test Data.
   Test Data:
   Country Postal Code Place Name
   us 90210 Beverly Hills
   us 12345 Schenectady
   ca B2R Waverley

• Verify that the response status code is 200 and content type is JSON.<br><br>
• Verify that the response time is below 1s.<br><br>
• Verify in Response - "Place Name" for each input "Country" and "Postal Code".




